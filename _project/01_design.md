---
slug: design
title: Project Design & Planning
---

- **REQUIRED:** Presentation:
    * Prepare a brief (<=2 min, <=2 slides) presentation for the workshop. Do not simply read to them your answers to the above questions.
    * Explain what you are doing and why, how the project is structured, who the users are and what technologies it depends on.  Briefly mention similar or related projects.
    * Use at least one visualization (e.g., the flowchart you develop in response to the prompt below) to help explain your project.
- Identify project scope:
    * In 1-3 sentences, what does your project *do* now?
    * Imagine that you have completed this workshop, including the hack-a-thon on your project; write the new version of those 1-3 sentences. Keep in mind: that description might not change at all!  If it won't change, write 1-3 sentences about what *will* change (e.g., performance). 
    * Are there similar projects (that is, performing mostly the same function, but perhaps in a different language or for different inputs) you can learn from or build on?  Do an internet search as needed.
    * Who will interact with your software?  Think broadly about the definition of "interact" - for example, who might see the results? Who else might change the code? Who might run the software?
    * What constraints are there on the user interface?
- I/O requirements:
    * What data does your project require (inputs)?
    * Where/how will you obtain it?
    * What are the outputs from your project?
    * What data/file formats will you need to work with?
- Divide and conquer:
    * Identify 3-6 subproblems that your project can be divided into
    * What is the most natural order in which to tackle the subproblems?
        * Easist first?  Hardest?  From the interface in, or following the flow of information?
        * Which can be implemented independently?  Do some absolutely have to be completed first?
    * How will you verify that specific subproblems have been solved?
- Draw a flowchart of your research problem and its parts.
    * Identify which steps each of your detailed requirements apply to.
    * Are the steps that do not have detailed requirements associated with them?
- Technical requirements:
    * What are the external technologies (for example, random number library, SQL database for recording data) your project uses? Or could use? Do an internet search as needed.
    * What do you need to know/learn in order to complete the project?
    * What platform does 
- If the project is substantially completed, what things do you think should have been done differently?
- Testing:
    * How could you test specific requirements have been satisfied?  As in, what comparisons would you make (not what framework would you use)?
    * To adequately & practically test your project, do you need to automate tests?
- Scientific Requirements:
    * What are the specific science-related concerns for your project?

<!--
1. Think about the following general questions
   * Why are you interested in this topic?  Why is this topic important?  
   * What question do you plan to answer?  Do you have a hypothesis yet?
   * What information/data do you need? How will you obtain it?
   * What subproblems do you anticipate (again consider part:whole, history, use)
   * How will you draw conclusions from that information? (What coding will you need to do?  What software will you use?)
   * What will your results be used for?
2. Outline/draw an overview of your research problem plan.
3. Conduct an initial review of literature and more precisely define your research problem.  Again, what subproblems do you foresee arising?
3. Identify Literature for Further Review
   * Summarize the results of previous research to form a foundation on which to build your own research
   * Collect ideas on how to gather data
   * Investigate methods of data analysis

*Students who have completed some research already should be honest about how they would do their project differently with aftersight.  He is expected to create a new proposal based on his research and open to feedback and constructive criticism.*

*No coding.* Initial work should be handwritten, then typed up (plaintext) as (at most) a draft outline for some sort of publication.  Students will present their research proposals at the end of the day.
-->

<!--nothing to add @percevalm-->
