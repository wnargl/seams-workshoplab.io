---
fullname: Jeremy Bingham
goby: Jeremy
img: Bingham.jpg
links:
  -
    title: Masters project repo
    url: https://github.com/JemJenn/Risk-Incidence
  -
    title: Social modelling
    url: https://github.com/JemJenn/Own_Social_Models
affiliation:
  -
    org: Stellenbosch University / SACEMA  
    position: Masters Student
---